#!/usr/bin/env bash
#
# Send an alert to Opsgenie, https://docs.opsgenie.com/docs/alert-api
#
# Required globals:
#   GENIE_KEY
#   MESSAGE
#
# Optional globals:
#   DESCRIPTION (default: Alert from <a href='https://bitbucket.org/${BITBUCKET_WORKSPACE}/${BITBUCKET_REPO_SLUG}/addon/pipelines/home#!/results/${BITBUCKET_BUILD_NUMBER}'>Pipeline #${BITBUCKET_BUILD_NUMBER}</a>)
#   SOURCE (default: Bitbucket Pipelines)
#   PRIORITY (default: P3)
#   DEBUG

source "$(dirname "$0")/common.sh"

extra_args=""
if [[ "${DEBUG}" == "true" ]]; then
  extra_args="--verbose"
fi

# mandatory variables
GENIE_KEY=${GENIE_KEY:?'GENIE_KEY variable missing.'}
MESSAGE=${MESSAGE:?'MESSAGE variable missing.'}

# default variables
DESCRIPTION=${DESCRIPTION:="Alert from <a href='https://bitbucket.org/${BITBUCKET_WORKSPACE}/${BITBUCKET_REPO_SLUG}/addon/pipelines/home#!/results/${BITBUCKET_BUILD_NUMBER}'>Pipeline #${BITBUCKET_BUILD_NUMBER}</a>"}
SOURCE=${SOURCE:='Bitbucket Pipelines'}
PRIORITY=${PRIORITY:='P3'}

info "Sending alert to Opsgenie..."

curl_output_file="/tmp/$RANDOM.txt"

payload=$(jq -n \
  --arg MESSAGE "${MESSAGE}" \
  --arg DESCRIPTION "${DESCRIPTION}" \
  --arg SOURCE "${SOURCE}" \
  --arg PRIORITY "${PRIORITY}" \
'{
    "message": $MESSAGE,
    "description": $DESCRIPTION,
    "source": $SOURCE,
    "priority": $PRIORITY
}')

run curl -s -X POST --output ${curl_output_file} -w "%{http_code}" \
    -H "Content-Type: application/json" \
    -H "Authorization: GenieKey ${GENIE_KEY}" \
    -d "$payload" \
    ${extra_args} \
    https://api.opsgenie.com/v2/alerts

response=$(cat $output_file)

info "HTTP Response: $(echo $response)"


# From https://docs.opsgenie.com/docs/alert-api:
# Create alert requests are processed asynchronously, therefore valid requests are responded with HTTP status 202 - Accepted.
if [[ "${response}" = 2* ]]; then
  success "Notification successful."

  request_id=$(cat $curl_output_file | jq -r .requestId)
  count=1
  while  [ -z "${status}" ] || [ "${status}" != "Created alert" ] && [ $count -le 60 ]; do
            
            run curl -s -X GET --output ${curl_output_file} \
                -H "Content-Type: application/json" \
                -H "Authorization: GenieKey ${GENIE_KEY}" \
                https://api.opsgenie.com/v2/alerts/requests/${request_id}

            
            debug "Checking request status, attempt number ${count} ..."
            status=$(cat $curl_output_file | jq -r .data.status)
            sleep 1
            ((count++))
        done
  if [ -z "${status}" ]; then
    error "Unable to get the status of the request: ${requests_id}"
  else
    alert_id=$(cat $curl_output_file | jq -r .data.alertId)
    info "Follow this link to watch the alert in the Opsgenie UI: https://app.opsgenie.com/alert/detail/${alert_id}/details"
  fi
else
  fail "Notification failed."
fi
