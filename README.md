# Bitbucket Pipelines Pipe: Opsgenie send alert

Sends an alert to [Opsgenie](https://www.opsgenie.com).

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: atlassian/opsgenie-send-alert:0.4.2
  variables:
    GENIE_KEY: '<string>'
    MESSAGE: '<string>'
    # DESCRIPTION: '<string>' # Optional.
    # SOURCE: '<string>' # Optional.
    # PRIORITY: '<string>' # Optional.
    # DEBUG: '<boolean>' # Optional.
```

## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| GENIE_KEY (*)         | API key. It is recommended to use a secure repository variable. |
| MESSAGE (*)           | Alert message. |
| DESCRIPTION           | Alert description. Default: `Alert from <a href='https://bitbucket.org/${BITBUCKET_WORKSPACE}/${BITBUCKET_REPO_SLUG}/addon/pipelines/home#!/results/${BITBUCKET_BUILD_NUMBER}'>Pipeline #${BITBUCKET_BUILD_NUMBER}</a>`. |
| SOURCE                | Alert source. Default: `Bitbucket Pipelines`. |
| PRIORITY              | Alert priority. Valid options are `P1`, `P2`, `P3`, `P4`, and `P5`. Default: `P3`. |
| DEBUG                 | Turn on extra debug information. Default: `false`. |

_(*) = required variables._

More info about parameters and values can be found in the Opsgenie official documentation: https://docs.opsgenie.com/docs/alert-api#section-create-alert.

## Prerequisites

To send alerts to Opsgenie, you need an API key. You can follow the instructions [here](https://docs.opsgenie.com/docs/api-integration) to create one.

## Examples

Basic example:

```yaml
script:
  - pipe: atlassian/opsgenie-send-alert:0.4.2
    variables:
      GENIE_KEY: $GENIE_KEY
      MESSAGE: 'Hello, world!'
```

Advanced example:

```yaml
script:
  - pipe: atlassian/opsgenie-send-alert:0.4.2
    variables:
      GENIE_KEY: $GENIE_KEY
      MESSAGE: 'Hello, world!'
      DESCRIPTION: 'An Opsgenie alert sent from Bitbucket Pipelines'
      SOURCE: 'Bitbucket Pipelines'
      PRIORITY: 'P1'
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce

## License
Copyright (c) 2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.

[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-pipelines-questions?add-tags=pipes,opsgenie
